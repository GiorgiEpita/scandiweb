<?php 

trait Furniture
{
    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function setLength($length)
    {
        $this->length = $length;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }
}

?>