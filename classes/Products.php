<?php
include_once "classes/DB.php";

abstract class Products
{
    use furniture;

    public $table = 'product';
    public $barcode;
    public $name;
    public $price;
    public $image;
    protected $type;
    protected $width;
    protected $length;
    protected $height;

    // SET Parametres
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    abstract function setType($type);

    // Read Data
    public function readAll()
    {
        $sql = "SELECT * FROM $this->table";
        $stmt = DB::prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    // Create Data
    public function insert()
    {
        $sql = "INSERT INTO $this->table(barcode, name, price, type, width, length, height, image)VALUES(:barcode, :name, :price, :type, :width, :length, :height, :image)";
    
        $stmt = DB::prepare($sql);
        $stmt->bindParam(':barcode', $this->barcode);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':price', $this->price);
        $stmt->bindParam(':type', $this->type);
        $stmt->bindParam(':width', $this->width);
        $stmt->bindParam(':length', $this->length);
        $stmt->bindParam(':height', $this->height);
        $stmt->bindParam(':image', $this->image);

        return $stmt->execute();
    }

    // Delete Data
    public function delete(array $id)
    {
        $placeholders = trim(str_repeat('?,', count($id)), ',');
        $sql = "DELETE FROM $this->table WHERE id IN ($placeholders)";
        $stmt = DB::prepare($sql);
        return $stmt->execute($id);
    }
}

?>